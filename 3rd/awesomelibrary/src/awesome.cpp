#include "awesome.h"


#include <iostream>
#include <cmath>

double ::awesome::mySqrt( double number )
{
    std::cout << "My Awesome implementation of square root\n";
    return std::sqrt( number );
}
