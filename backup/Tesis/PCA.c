
// This program carries out PCA analysis on a set of 6 bitmap images.
// The images contain objects against blank backgrounds.
// The eigenvalues and eigenvectors for the set of images are calculated
// and based on these decomposition coefficients are calculated for each image.

// The images are compared in terms of their decomposition coefficient vectors.
// I'm not sure if this is the correct thing to do, but vectors for similar images
// seemed to have similar values. I assume they represent the location of each image
// in the eigen space, in this case, a 5-d space. The similarity between images is
// calculated by simply getting the distance in each dimension

// So that each value in the vectors are given equal importance in comparing images,
// the vectors are normalised, i.e. for each dimension the values for each image
// are converted to values between 1 and -1. Again I amd not sure if this is the
// correct thing to do, but some dimensions came up with huge values that made
// values in the other dimensions inconsequent.

// N.B. When the images are projected into the eigen space, the images come
// out as blank white or black, however, when I use images captured from a
// webcam, the images come out fine. Dont know why.



//#include "stdafx.h"
#include "cv.h"  // include core library interface
#include "highgui.h" // include GUI library interface
#include "cvaux.h"
#include <stdio.h>
#include <cmath>
#include <string.h>
#include <ctime>


int main()
{
	const int n = 10;				// number of images used
	const int nEigens = n-1;		// number of eigen objs
	int i, j, k, digit = 10;
	char name[32] ;	// 6 bmp images
	char nums[7] = "012345";		// each image has a different number
	double max, val, dist;			// dist between images in nEigens-d space
	FILE* outFile;					// file to store results
	CvMemStorage* storage = cvCreateMemStorage(0);

	IplImage* images[n];			// input images
	IplImage* eigens[nEigens];		// eigenobjects
	IplImage* proj[n];				// decomposed object projection to the eigen sub-space
	IplImage* avg;					// averaged object
	CvTermCriteria criteria;		// criteria to stop calculating eigen objects
	float vals[nEigens+1];			// eigenvalues
	float coeffs[n][nEigens];		// decomposition coefficients
	float normCoeffs[n][nEigens];	// normalised coefficients

	cvvNamedWindow( "projections", 1);
	cvvNamedWindow( "images", 1);




	// ************initialise variables************
	if((outFile = fopen("values.txt", "w")) == NULL)
		printf(" error creating file\n");

	for( i=0; i<n; i++ ){
		//name[i] = nums[i]; 			// get file name of images
                sprintf(name, "mask%d.jpg", i);
		IplImage* temp = cvLoadImage( name ); 	// load images into IplImage objects

		images[i] = cvCreateImage( cvGetSize( temp ), IPL_DEPTH_8U, 1 );
		//cvCvtColor( temp, images[i], CV_BGR2GRAY );
		cvReleaseImage( &temp );

		cvvShowImage( "images", images[i] );
		cvvWaitKey(0);

		proj[i] = cvCreateImage( cvGetSize( images[0] ), IPL_DEPTH_8U, 1 );
	}

	for( i=0; i<nEigens; i++ )
		eigens[i] = cvCreateImage( cvGetSize( images[0] ), IPL_DEPTH_32F, 1 );

	avg = cvCreateImage( cvGetSize( images[0] ), IPL_DEPTH_32F, 1 );
	criteria.type = CV_TERMCRIT_ITER|CV_TERMCRIT_EPS;
    criteria.max_iter = 10;
    criteria.epsilon = 0.1;



	// ************calc eigenobjects & eigenvals************
	cvCalcEigenObjects( n, images, eigens, 0, 0, 0, &criteria, avg, vals );

	fprintf(outFile, "eigenvalues\n" );

	for( i=0; i<nEigens; i++ )
		fprintf(outFile, "%15.2lf\n", vals[i] );



	// ************calc decomposition coefficients************
	fprintf(outFile, "decomposition coefficients\n" );

	for( i=0; i<n; i++ ){
		cvEigenDecomposite( images[i], nEigens, eigens, 0, 0, avg, coeffs[i] );

		fprintf(outFile, "obj:%d\t", i );

		for( j=0; j<nEigens; j++ )
			fprintf(outFile, "%25.2lf", coeffs[i][j] );

		fprintf(outFile, "\n" );
	}

	// ************normalise decomposition coefficients************
	for( i=0; i<nEigens; i++ ){
		max = -100000.00;

		for( j=0; j<n; j++ ){
			val = fabs( coeffs[j][i] );

			if( val > max )
				max = val;
		}
		for( j=0; j<n; j++ )
			normCoeffs[j][i]=coeffs[j][i]/max;
	}

	fprintf(outFile, "decomposition coefficients-normalised\n" );

	for( i=0; i<n; i++ ){
		fprintf(outFile, "obj:%d\t", i );

		for( j=0; j<nEigens; j++ )
			fprintf(outFile, "%7.2lf", normCoeffs[i][j] );

		fprintf(outFile, "\n" );
	}


	// ************compare vectors************
	for( i=0; i<n; i++ ){
		fprintf(outFile, "\nvector %d\n", i );

		for( j=i+1; j<n; j++ ){
			dist = 0;

			for( k=0; k<nEigens; k++ )
				dist += fabs( normCoeffs[i][k] - normCoeffs[j][k] );

			fprintf(outFile, "%d:\tdist = %10.5lf\n", j, dist );
		}
	}


	// ************calc projection into the eigen subspace************
	for( i=0; i<n; i++ )
		cvEigenProjection( eigens, nEigens, 0, 0, coeffs[i], avg, proj[i] );

	for( i=0; i<n; i++ ){
		cvvShowImage( "projections", proj[i] );
		cvvWaitKey(0);
	}


	// ************tidy up************
	for( i=0; i<n; i++ ){
		cvReleaseImage( &images[i] );
		cvReleaseImage( &proj[i] );
	}

	for( i=0; i<nEigens; i++ )
		cvReleaseImage( &eigens[i] );

	cvReleaseImage( &avg );

	cvClearMemStorage( storage );

	return 0;
}
