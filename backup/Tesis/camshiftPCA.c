#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "cv.h"
#include "highgui.h"
#include "cvaux.h"
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <string.h>
#include <ctime>
#endif

// Definicion de Imagenes Camshift
IplImage *image = 0, *hsv = 0, *hue = 0, *sat = 0, *val = 0, *mask = 0,  *backproject = 0, *tmp = 0, *maskara = 0, *temp = 0, *temporal = 0, *imagegray = 0, *resize = 0, *invert = 0; 



// Definicion de Histogramas
CvHistogram *hist = 0;

// Deficion elemento estructurante para erosion y dilatacion
IplConvKernel* element = 0;
const int element_shape = CV_SHAPE_ELLIPSE;//CV_SHAPE_RECT;//CV_SHAPE_ELLIPSE;CV_SHAPE_CROSS
// Elementos conectdos entregado por floodfill
CvConnectedComp comp;

// Definicion de Variables Camshift
int backproject_mode = 0;
int select_object = 0;
int track_object = 0;
CvSize size = cvSize( 101, 101 );
CvPoint origin;
CvPoint seedlast; 
CvRect selection;
CvRect track_window;
CvBox2D track_box;
CvConnectedComp track_comp;
int vmin = 10, vmax = 256, smin = 30;
int h_bins = 30, s_bins = 32;
int hist_size[] = {h_bins, s_bins};
float h_ranges[] = { 0, 180 }; /* hue varies from 0 (~0°red) to 180 (~360°red again) */
float s_ranges[] = { 0, 255 }; /* saturation varies from 0 (black-gray-white) to 255 (pure spectrum color) */
float* ranges[] = { h_ranges, s_ranges };
int scale = 10;        
int label = 0;
int clave = 0;

int new_mask_val = 255;
int connectivity = 8;
int flags = connectivity + (new_mask_val << 8) + (0);

// *******************Definicion de Variables PCA****************

const int nGest = 14;		// number of images used
const int nEigens = nGest-1;        // number of eigen objs
int l, t, nun = nGest, mun = nGest-1;int i, j, k;
char name[32] ;	// 6 bmp images
double max, valor, dist;			// dist between images in nEigens-d space
FILE* outFile;					// file to store results
CvMemStorage* storage = cvCreateMemStorage(0);
CvTermCriteria criteria;
float vals[nEigens+1];			// eigenvalues
float coeffs[nGest][nEigens];		// decomposition coefficients
float normCoeffs[nGest][nEigens];	// normalised coefficients
float rescoeff[nGest];
float test_vals[nEigens]; // eigenvalues
float test_coeffs[nEigens]; // decomposition coefficients
float test_normCoeffs[nEigens]; // normalised coefficients
double Dista;
double ref; 
int gesto = 100;
char Gesto[255];


//***********************************************************
// Definicion de Imagenes Pca
IplImage* images[nGest];			// input images
IplImage* eigens[nEigens];		// eigenobjects
IplImage* proj[nGest];	        // decomposed object projection to the eigen sub-space
IplImage* avg;	// Imagen Promedio
IplImage* testsource; // imagen a comparar
//IplImage* test_eigens[nEigens];

void on_mouse( int event, int x, int y, int flags, void* param )
{
    if( !image )
        return;

    if( image->origin )
        y = image->height - y;

    if( select_object )
    {
        selection.x = MIN(x,origin.x);
        selection.y = MIN(y,origin.y);
        selection.width = selection.x + CV_IABS(x - origin.x);
        selection.height = selection.y + CV_IABS(y - origin.y);
        
        selection.x = MAX( selection.x, 0 );
        selection.y = MAX( selection.y, 0 );
        selection.width = MIN( selection.width, image->width );
        selection.height = MIN( selection.height, image->height );
        selection.width -= selection.x;
        selection.height -= selection.y;
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        origin = cvPoint(x,y);
        selection = cvRect(x,y,0,0);
        select_object = 1;
        break;
    case CV_EVENT_LBUTTONUP:
        select_object = 0;
        if( selection.width > 0 && selection.height > 0 )
            track_object = -1;
        break;
    }
}


CvScalar hsv2rgb( float hue )
{
    int rgb[3], p, sector;
    static const int sector_data[][3]=
        {{0,2,1}, {1,2,0}, {1,0,2}, {2,0,1}, {2,1,0}, {0,1,2}};
    hue *= 0.033333333333333333333333333333333f;
    sector = cvFloor(hue);
    p = cvRound(255*(hue - sector));
    p ^= sector & 1 ? 255 : 0;

    rgb[sector_data[sector][0]] = 255;
    rgb[sector_data[sector][1]] = 0;
    rgb[sector_data[sector][2]] = p;

    return cvScalar(rgb[2], rgb[1], rgb[0],0);
}

int main( int argc, char** argv )
{
    CvCapture* capture = 0;
    
    if( argc == 1 || (argc == 2 && strlen(argv[1]) == 1 && isdigit(argv[1][0])))
        capture = cvCaptureFromCAM( argc == 2 ? argv[1][0] - '0' : 0 );
    else if( argc == 2 )
        capture = cvCaptureFromAVI( argv[1] ); 

    if( !capture )
    {
        fprintf(stderr,"Could not initialize capturing...\n");
        return -1;
    }

    printf( "Hot keys: \n"
        "\tESC - quit the program\n"
        "\tc - stop the tracking\n"
        "\tb - switch to/from backprojection view\n"
        "To initialize tracking, select the object with mouse\n" );

    //cvNamedWindow( "Backprojection", 1 );
    cvNamedWindow( "CamShiftDemo", 1 );
    cvNamedWindow( "mask", 1 );
    //cvNamedWindow( "hue", 1 );
    //cvNamedWindow( "sat", 1 );
    //cvNamedWindow( "Floodfill", 1 );
    cvSetMouseCallback( "CamShiftDemo", on_mouse, 0 );
    cvCreateTrackbar( "Vmin", "CamShiftDemo", &vmin, 256, 0 );
    cvCreateTrackbar( "Vmax", "CamShiftDemo", &vmax, 256, 0 );
    cvCreateTrackbar( "Smin", "CamShiftDemo", &smin, 256, 0 );

//***************************************************************************
       CvMat** inputMatrix = new CvMat*[nun];
       for( l = 0; l < nun; l++ ){
       inputMatrix[l]=cvCreateMat(1,mun,CV_64FC1);}

       CvMat *outputMatrix=cvCreateMat(mun,mun,CV_64FC1);
       CvMat *inverseMatrix=cvCreateMat(mun,mun,CV_64FC1);
       CvMat *meanVector=cvCreateMat(1,mun,CV_64FC1);
       CvMat *Distmah = cvCreateMat(1,nun,CV_64FC1);
       CvMat *Testvector=cvCreateMat(1,mun,CV_64FC1);

        if((outFile = fopen("valuesmod.txt", "w")) == NULL)
	      printf(" error creating file\n");

	for( i=0; i<nGest; i++ ){
		sprintf(name, "muestra/muestra1/mask%d.jpg", i);// get file name of images
		IplImage* temp = cvLoadImage( name ); 	// load images into IplImage objects
                images[i] = cvCreateImage( cvGetSize( temp ), IPL_DEPTH_8U, 1 );
		cvCvtColor( temp, images[i], CV_BGR2GRAY );
                
		//cvShowImage( "images", images[i] );
		//cvWaitKey(0);

		proj[i] = cvCreateImage( cvGetSize( temp ), IPL_DEPTH_8U, 1 );
                cvReleaseImage( &temp );
	}

	for( i=0; i<nEigens; i++ )
		eigens[i] = cvCreateImage( cvGetSize( images[0] ), IPL_DEPTH_32F, 1 );

	        avg = cvCreateImage( cvGetSize( images[0] ), IPL_DEPTH_32F, 1 );
	        criteria.type = CV_TERMCRIT_ITER|CV_TERMCRIT_EPS;
                criteria.max_iter = 1000;
                criteria.epsilon = 0.1;

	// ************calc eigenobjects & eigenvals************
	cvCalcEigenObjects( nGest, images, eigens, 0, 0, 0, &criteria, avg, vals );
        
	fprintf(outFile, "eigenvalues\n" );
        //printf( "eigenvalues\n" );
	for( i=0; i<nEigens; i++ )
		{fprintf(outFile, "%15.2lf\n", vals[i] );
                 //printf("%15.2lf\n", vals[i] );
                char filename1[32];
                char filename2[32];
                
		sprintf(filename1, "eigen%d.jpg", i);
                sprintf(filename2, "average.jpg");
                
		cvSaveImage(filename1, eigens[i]);
                cvSaveImage(filename2, avg);}
                


	// ************calc decomposition coefficients************
	//fprintf(outFile, "decomposition coefficients\n" );

	for( i=0; i<nGest; i++ ){
		cvEigenDecomposite( images[i], nEigens, eigens, 0, 0, avg, coeffs[i] );

		fprintf(outFile, "obj:%d\t", i );

		for( j=0; j<nEigens; j++ )
			fprintf(outFile, "%25.2lf", coeffs[i][j] );

		fprintf(outFile, "\n" );
	}

        for( l = 0; l < nun; l++ )
        {
         for( t = 0; t < mun; t++ )
             cvmSet(inputMatrix[l],0,t,coeffs[l][t]);
             //cvmSet(inputMatrix[l],0,t,normCoeffs[l][t]);
             //cvmSet(inputMatrix[l],0,t,cvRandReal(&rng)*10-5);
        }
cvCalcCovarMatrix((const void**)inputMatrix,nun,outputMatrix,meanVector,CV_COVAR_NORMAL+CV_COVAR_SCALE);

//printf( "det(cov)=%g\n", cvDet(outputMatrix) );

cvInvert(outputMatrix,inverseMatrix,CV_SVD);
//***************************************************************************
    for(;;)
    {
        IplImage* frame = 0;
        int i, bin_w, c;

        frame = cvQueryFrame( capture );
        if( !frame )
            break;

        if( !image )
        {
            /* Definicion de Imagenes y Histograma */
            image = cvCreateImage( cvGetSize(frame), 8, 3 );
            imagegray = cvCreateImage( cvGetSize(frame), 8, 1 );
            resize = cvCreateImage( size, IPL_DEPTH_8U, 1 );
            invert = cvCreateImage( size, 8, 1 );
            image->origin = frame->origin;
            hsv = cvCreateImage( cvGetSize(frame), 8, 3 );
            hue = cvCreateImage( cvGetSize(frame), 8, 1 );
            sat = cvCreateImage( cvGetSize(frame), 8, 1 );
            val = cvCreateImage( cvGetSize(frame), 8, 1 );
            mask = cvCreateImage( cvGetSize(frame), 8, 1 );
            backproject = cvCreateImage( cvGetSize(frame), 8, 1 );
            hist = cvCreateHist( 2, hist_size, CV_HIST_ARRAY, ranges, 1 );
            
          
        }
        // cambio de RGB a HSV
        cvCopy( frame, image, 0 );
        cvCvtColor( image, imagegray, CV_BGR2GRAY );
        cvCvtColor( image, hsv, CV_BGR2HSV );

        if( track_object )
        {
            int _vmin = vmin, _vmax = vmax;

            cvInRangeS( hsv, cvScalar(0,smin,MIN(_vmin,_vmax),0),
                        cvScalar(180,256,MAX(_vmin,_vmax),0), mask );
            
            // Separacion de planos HSV
            cvCvtPixToPlane(hsv, hue, sat, val, 0);
            //cvShowImage( "hue", hue );
            //cvShowImage( "sat", sat);
            IplImage *planes[] = {hue, sat};

            if( track_object < 0 )
            {
                float max_val = 0.f;
                float min_val = 0.f;
                
                // Deficicion de region de interes
                cvSetImageROI( hue, selection );
                cvSetImageROI( sat, selection );
                cvSetImageROI( mask, selection );
                
                //Calculo Histograma
                cvCalcHist( planes, hist, 0, mask );
                cvGetMinMaxHistValue( hist, &min_val, &max_val, 0, 0 );
                cvConvertScale( hist->bins, hist->bins, max_val ? 255. / max_val : 0., 0 );
                
                // Liberacion de zona de Interes
                cvResetImageROI( hue );
                cvResetImageROI( sat );
                cvResetImageROI( mask );

                
                track_window = selection;
                track_object = 1;
                
                
            }

            // Calcular backprojection
            cvCalcBackProject( planes, backproject, hist );
            cvAnd( backproject, mask, backproject, 0 );
            
            //Erosion y Dilatacion
            element = cvCreateStructuringElementEx( 3, 3, 1, 1, element_shape, 0 );
            cvErode( backproject, backproject, element, 1 );
            cvDilate( backproject, backproject, element, 2);
            //cvShowImage( "Backprojection", backproject );

            // Genera imagen original despues de aplicar backprojection
            tmp = cvCloneImage(image);
            cvZero(tmp);
            cvCopy(image, tmp , backproject);
            temp = cvCloneImage(tmp);
            //cvShowImage( "mask", tmp);

            // Algoritmo Camshift
            cvCamShift( backproject, track_window,
            cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ),
            &track_comp, &track_box );
            track_window = track_comp.rect;
            CvScalar color = CV_RGB( 0, 0, 255);

            maskara = cvCreateImage( cvSize(image->width + 2 , image->height + 2 ), 8, 1 );
            cvZero( maskara );

            // Algoritmo Floodfill
            CvPoint seed = cvPointFrom32f( track_box.center );
//*******************
            
            //printf("x =%d, y=%d\n", seed.x, seed.y);
if ( seed.x > 20 && seed.x < image->width-20 && seed.y > 20 && seed.y < image->height-20 ){
            CvScalar s1, s2;
            s1=cvGet2D(tmp, seed.y, seed.x ); // get the (i,j) pixel value
            //printf("intensity=%f\n",s1.val[0]);
            if (s1.val[0] > 0)
               {seedlast.x = seed.x;
                seedlast.y = seed.y;}
            if (s1.val[0] == 0)
               {seed.x = seedlast.x;
                seed.y = seedlast.y;
                s2=cvGet2D(tmp, seed.y, seed.x );
while (s2.val[0] == 0 && seed.x > 20 && seed.x < image->width-20 && seed.y > 20 && seed.y < image->height-20){
                seed.x = seed.x-1;
                seed.y = seed.y-1;
                s2=cvGet2D(tmp, seed.y, seed.x );}
                }
               }
//*****************
            cvFloodFill(tmp, seed, color, CV_RGB( 20, 20, 20), CV_RGB( 80, 80, 80 ), &comp, flags,  maskara );  
            // Genera imagen original despues de aplicar floodfill
            temporal = cvCloneImage(imagegray);
            temp = cvCreateImage( cvGetSize(frame), 8, 1 );
            cvZero(temporal);
            cvResize(maskara, temp);
            cvCopy(imagegray, temporal , temp);
            //temp = cvCloneImage(tmp);
            cvShowImage( "mask", temporal);

            cvSetImageROI( maskara, comp.rect);
            cvSetImageROI( temporal, comp.rect);
            cvResize(temporal, resize);
            cvNot( resize, invert );
            //cvShowImage( "Floodfill", maskara);
            cvResetImageROI( maskara );
            cvResetImageROI( temporal );

//*************************************************
//testsource = cvCreateImage( cvGetSize( resize ), IPL_DEPTH_8U, 1);

//cvCvtColor( resize, testsource, CV_BGR2GRAY );

cvEigenDecomposite( resize, nEigens, eigens, 0, 0, avg, test_coeffs );

for( t = 0; t < mun; t++ )
cvmSet(Testvector,0,t,test_coeffs[t]);
//cvmSet(Testvector,0,t,test_normCoeffs[t]);
//cvmSet(inputMatrix[l],0,t,cvRandReal(&rng)*10-5);
//}
fprintf(outFile, "Distancia Mahalonobis\n");
//printf( "Distancia Mahalonobis\n");
ref = 1000;
for( l = 0; l < nun; l++ )
{
      Dista = cvMahalanobis(Testvector,inputMatrix[l],inverseMatrix);
      cvmSet(Distmah,0,l,Dista);
      //printf( "d(v[test],v[%d])=%g\n", l,Dista);
      fprintf(outFile,"d(v[test],v[%d])=%g\n", l,Dista);
if(ref > Dista){
ref = Dista;
gesto = l;}
}

if (ref > 1.7){
gesto = 20;
}
//fprintf( "d(v[test],v[%d])=%g\n", gesto,Dista);
//*************************************************
            if( backproject_mode )
                cvCvtColor( backproject, image, CV_GRAY2BGR );
            if( image->origin )
            track_box.angle = -track_box.angle;
            cvEllipseBox( image, track_box, CV_RGB(255,0,0), 3, CV_AA, 0 );
        }
        
        if( select_object && selection.width > 0 && selection.height > 0 )
        {
            cvSetImageROI( image, selection );
            cvXorS( image, cvScalarAll(255), image, 0 );
            cvResetImageROI( image );
        }
//**********************************************************+        
        switch(gesto){
        case 0:
            sprintf(Gesto, "Agarra");
            break;
        case 1:
            sprintf(Gesto, "la ola");
            break;
        case 2:
            sprintf(Gesto, "Rock");
            break;
        case 3:
            sprintf(Gesto, "OK");
            break;
        case 4:
            sprintf(Gesto, "Dos");
            break;
        case 5:
            sprintf(Gesto, "Tres");
            break;
        case 6:
            sprintf(Gesto, "Cuatro");
            break;
        case 7:
            sprintf(Gesto, "FOUR");
            break;
        case 8:
            sprintf(Gesto, "OKI DOKI");
            break;
        case 9:
            sprintf(Gesto, "ET");
            break;
        case 10:
            sprintf(Gesto, "Izquierda");
            break;
        case 11:
            sprintf(Gesto, "Derecha");
            break;
        case 12:
            sprintf(Gesto, "PAZ");
            break;
        case 13:
            sprintf(Gesto, "Mano");
            break;
        case 20:
            sprintf(Gesto, "No Recognition");
            break;
        default:
            ;
        }

//***************************************************************
/*//**********************************************************+        
        switch(gesto){
        case 0:
            sprintf(Gesto, "A");
            break;
        case 1:
            sprintf(Gesto, "B");
            break;
        case 2:
            sprintf(Gesto, "G");
            break;
        case 3:
            sprintf(Gesto, "D");
            break;
        case 4:
            sprintf(Gesto, "E");
            break;
        case 5:
            sprintf(Gesto, "H");
            break;
        case 6:
            sprintf(Gesto, "I");
            break;
        case 7:
            sprintf(Gesto, "K");
            break;
        case 8:
            sprintf(Gesto, "L");
            break;
        case 9:
            sprintf(Gesto, "M");
            break;
        case 10:
            sprintf(Gesto, "N");
            break;
        case 11:
            sprintf(Gesto, "P");
            break;
        case 12:
            sprintf(Gesto, "Q");
            break;
        case 13:
            sprintf(Gesto, "R");
            break;
       case 14:
            sprintf(Gesto, "S");
            break;
        case 15:
            sprintf(Gesto, "U");
            break;
        case 16:
            sprintf(Gesto, "V");
            break;
        case 17:
            sprintf(Gesto, "W");
            break;
        case 18:
            sprintf(Gesto, "Y");
            break;
        case 19:
            sprintf(Gesto, "Z");
            break;
        case 20:
            sprintf(Gesto, "1");
            break;
        case 21:
            sprintf(Gesto, "3");
            break;
        case 22:
            sprintf(Gesto, "4");
            break;
        case 23:
            sprintf(Gesto, "5");
            break;
        case 24:
            sprintf(Gesto, "6");
            break;
        case 25:
            sprintf(Gesto, "8");
            break;
        case 26:
            sprintf(Gesto, "0");
            break;
        case 30:
            sprintf(Gesto, "No Recognition");
            break;
        default:
            ;
        }

//****************************************************************/



        // Setup a simple font, and write text to the image.
        CvFont font;
        cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.8, 0.8, 0, 2);
        //sprintf(Gesto, "d(v[test],v[%d])=%g\n", gesto, Dista);
        cvPutText(image, Gesto, cvPoint(80, 260), &font, cvScalar(0, 0, 300));
        
        cvShowImage( "CamShiftDemo", image );
        

/* Salvar imagenes a un archivo       */


int keyCode = cvWaitKey(10);
if ('k' == (char)keyCode)
{label = 1;}
if (label == 1)
		{	
			static int imageCount = 0;
			char filename1[32];
                        char filename2[32];
                        char filename3[32];
			sprintf(filename1, "muestracam/image%d.jpg", imageCount);
                        sprintf(filename2, "muestracam/back%d.jpg", imageCount);
                        sprintf(filename3, "muestracam/mask%d.jpg", imageCount);
			cvSaveImage(filename1, image);
                        cvSaveImage(filename2, invert);
                        cvSaveImage(filename3, resize);
			++imageCount;}

/*      */

        cvReleaseImage( &tmp);
        cvReleaseImage( &temp);
        cvReleaseImage( &temporal);
        cvReleaseImage( &maskara);
        

        c = cvWaitKey(10);
        if( c == 27 )
            break;
        switch( c )
        {
        case 'b':
            backproject_mode ^= 1;
            break;
        case 'c':
            track_object = 0;
            break;
        
        default:
            ;
        }
    }

    cvReleaseCapture( &capture );
    cvDestroyWindow("CamShiftDemo");
    cvDestroyWindow( "Backprojection" );
    cvDestroyWindow( "mask" );
    cvDestroyWindow( "hue" );
    cvDestroyWindow( "sat" );
    cvDestroyWindow( "Floodfill" );

    return 0;
}

#ifdef _EiC
main(1,"Camshift2Dfinal.c");
#endif
