#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "cv.h"
#include "highgui.h"
#include <stdio.h>
#include <ctype.h>
#endif

// Definicion de Imagenes
IplImage *image = 0, *hsv = 0, *hue = 0, *sat = 0, *val = 0, *mask = 0,  *backproject = 0, *tmp = 0, *maskara = 0, *temp = 0, *temporal = 0, *imagegray = 0, *resize = 0; 

// Definicion de Histogramas
CvHistogram *hist = 0;

// Deficion elemento estructurante para erosion y dilatacion
IplConvKernel* element = 0;
const int element_shape = CV_SHAPE_ELLIPSE;//CV_SHAPE_RECT;//CV_SHAPE_ELLIPSE;CV_SHAPE_CROSS
// Elementos conectdos entregado por floodfill
CvConnectedComp comp;

// Definicion de Variables
int backproject_mode = 0;
int select_object = 0;
int track_object = 0;
CvSize size = cvSize( 50, 50 );
CvPoint origin;
CvRect selection;
CvRect track_window;
CvBox2D track_box;
CvConnectedComp track_comp;
int vmin = 10, vmax = 256, smin = 30;
int h_bins = 30, s_bins = 32;
int hist_size[] = {h_bins, s_bins};
float h_ranges[] = { 0, 180 }; /* hue varies from 0 (~0°red) to 180 (~360°red again) */
float s_ranges[] = { 0, 255 }; /* saturation varies from 0 (black-gray-white) to 255 (pure spectrum color) */
float* ranges[] = { h_ranges, s_ranges };
int scale = 10;        
int label = 0;


int new_mask_val = 255;
int connectivity = 8;
int flags = connectivity + (new_mask_val << 8) +
                        (0);

void on_mouse( int event, int x, int y, int flags, void* param )
{
    if( !image )
        return;

    if( image->origin )
        y = image->height - y;

    if( select_object )
    {
        selection.x = MIN(x,origin.x);
        selection.y = MIN(y,origin.y);
        selection.width = selection.x + CV_IABS(x - origin.x);
        selection.height = selection.y + CV_IABS(y - origin.y);
        
        selection.x = MAX( selection.x, 0 );
        selection.y = MAX( selection.y, 0 );
        selection.width = MIN( selection.width, image->width );
        selection.height = MIN( selection.height, image->height );
        selection.width -= selection.x;
        selection.height -= selection.y;
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        origin = cvPoint(x,y);
        selection = cvRect(x,y,0,0);
        select_object = 1;
        break;
    case CV_EVENT_LBUTTONUP:
        select_object = 0;
        if( selection.width > 0 && selection.height > 0 )
            track_object = -1;
        break;
    }
}


CvScalar hsv2rgb( float hue )
{
    int rgb[3], p, sector;
    static const int sector_data[][3]=
        {{0,2,1}, {1,2,0}, {1,0,2}, {2,0,1}, {2,1,0}, {0,1,2}};
    hue *= 0.033333333333333333333333333333333f;
    sector = cvFloor(hue);
    p = cvRound(255*(hue - sector));
    p ^= sector & 1 ? 255 : 0;

    rgb[sector_data[sector][0]] = 255;
    rgb[sector_data[sector][1]] = 0;
    rgb[sector_data[sector][2]] = p;

    return cvScalar(rgb[2], rgb[1], rgb[0],0);
}

int main( int argc, char** argv )
{
    CvCapture* capture = 0;
    
    if( argc == 1 || (argc == 2 && strlen(argv[1]) == 1 && isdigit(argv[1][0])))
        capture = cvCaptureFromCAM( argc == 2 ? argv[1][0] - '0' : 0 );
    else if( argc == 2 )
        capture = cvCaptureFromAVI( argv[1] ); 

    if( !capture )
    {
        fprintf(stderr,"Could not initialize capturing...\n");
        return -1;
    }

    printf( "Hot keys: \n"
        "\tESC - quit the program\n"
        "\tc - stop the tracking\n"
        "\tb - switch to/from backprojection view\n"
        "To initialize tracking, select the object with mouse\n" );

    //cvNamedWindow( "Backprojection", 1 );
    cvNamedWindow( "CamShiftDemo", 1 );
    cvNamedWindow( "mask", 1 );
    //cvNamedWindow( "hue", 1 );
    //cvNamedWindow( "sat", 1 );
    //cvNamedWindow( "Floodfill", 1 );
    cvSetMouseCallback( "CamShiftDemo", on_mouse, 0 );
    cvCreateTrackbar( "Vmin", "CamShiftDemo", &vmin, 256, 0 );
    cvCreateTrackbar( "Vmax", "CamShiftDemo", &vmax, 256, 0 );
    cvCreateTrackbar( "Smin", "CamShiftDemo", &smin, 256, 0 );

    for(;;)
    {
        IplImage* frame = 0;
        int i, bin_w, c;

        frame = cvQueryFrame( capture );
        if( !frame )
            break;

        if( !image )
        {
            /* Definicion de Imagenes y Histograma */
            image = cvCreateImage( cvGetSize(frame), 8, 3 );
            imagegray = cvCreateImage( cvGetSize(frame), 8, 1 );
            resize = cvCreateImage( size, 8, 1 );
            image->origin = frame->origin;
            hsv = cvCreateImage( cvGetSize(frame), 8, 3 );
            hue = cvCreateImage( cvGetSize(frame), 8, 1 );
            sat = cvCreateImage( cvGetSize(frame), 8, 1 );
            val = cvCreateImage( cvGetSize(frame), 8, 1 );
            mask = cvCreateImage( cvGetSize(frame), 8, 1 );
            backproject = cvCreateImage( cvGetSize(frame), 8, 1 );
            hist = cvCreateHist( 2, hist_size, CV_HIST_ARRAY, ranges, 1 );
            
          
        }
        // cambio de RGB a HSV
        cvCopy( frame, image, 0 );
        cvCvtColor( image, imagegray, CV_BGR2GRAY );
        cvCvtColor( image, hsv, CV_BGR2HSV );

        if( track_object )
        {
            int _vmin = vmin, _vmax = vmax;

            cvInRangeS( hsv, cvScalar(0,smin,MIN(_vmin,_vmax),0),
                        cvScalar(180,256,MAX(_vmin,_vmax),0), mask );
            
            // Separacion de planos HSV
            cvCvtPixToPlane(hsv, hue, sat, val, 0);
            //cvShowImage( "hue", hue );
            //cvShowImage( "sat", sat);
            IplImage *planes[] = {hue, sat};

            if( track_object < 0 )
            {
                float max_val = 0.f;
                float min_val = 0.f;
                
                // Deficicion de region de interes
                cvSetImageROI( hue, selection );
                cvSetImageROI( sat, selection );
                cvSetImageROI( mask, selection );
                
                //Calculo Histograma
                cvCalcHist( planes, hist, 0, mask );
                cvGetMinMaxHistValue( hist, &min_val, &max_val, 0, 0 );
                cvConvertScale( hist->bins, hist->bins, max_val ? 255. / max_val : 0., 0 );
                
                // Liberacion de zona de Interes
                cvResetImageROI( hue );
                cvResetImageROI( sat );
                cvResetImageROI( mask );

                
                track_window = selection;
                track_object = 1;
                
                
            }

            // Calcular backprojection
            cvCalcBackProject( planes, backproject, hist );
            cvAnd( backproject, mask, backproject, 0 );
            
            //Erosion y Dilatacion
            element = cvCreateStructuringElementEx( 3, 3, 1, 1, element_shape, 0 );
            cvErode( backproject, backproject, element, 1 );
            cvDilate( backproject, backproject, element, 2);
            //cvShowImage( "Backprojection", backproject );

            // Genera imagen original despues de aplicar backprojection
            tmp = cvCloneImage(image);
            cvZero(tmp);
            cvCopy(image, tmp , backproject);
            temp = cvCloneImage(tmp);
            //cvShowImage( "mask", tmp);

            // Algoritmo Camshift
            cvCamShift( backproject, track_window,
            cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ),
            &track_comp, &track_box );
            track_window = track_comp.rect;
            CvScalar color = CV_RGB( 0, 0, 255);

            maskara = cvCreateImage( cvSize(image->width + 2 , image->height + 2 ), 8, 1 );
            cvZero( maskara );

            // Algoritmo Floodfill
            CvPoint seed = cvPointFrom32f( track_box.center ); 
            cvFloodFill(tmp, seed, color, CV_RGB( 20, 20, 20), CV_RGB( 80, 80, 80 ), &comp, flags,  maskara );  
            // Genera imagen original despues de aplicar floodfill
            temporal = cvCloneImage(imagegray);
            temp = cvCreateImage( cvGetSize(frame), 8, 1 );
            cvZero(temporal);
            cvResize(maskara, temp);
            cvCopy(imagegray, temporal , temp);
            //temp = cvCloneImage(tmp);
            cvShowImage( "mask", temporal);

            cvSetImageROI( maskara, comp.rect);
            cvSetImageROI( temporal, comp.rect);
            cvResize(temporal, resize);
            //cvShowImage( "Floodfill", maskara);
            cvResetImageROI( maskara );
            cvResetImageROI( temporal );

            if( backproject_mode )
                cvCvtColor( backproject, image, CV_GRAY2BGR );
            if( image->origin )
            track_box.angle = -track_box.angle;
            cvEllipseBox( image, track_box, CV_RGB(255,0,0), 3, CV_AA, 0 );
        }
        
        if( select_object && selection.width > 0 && selection.height > 0 )
        {
            cvSetImageROI( image, selection );
            cvXorS( image, cvScalarAll(255), image, 0 );
            cvResetImageROI( image );
        }
        
        // Setup a simple font, and write text to the image.
        CvFont font;
        cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.8, 0.8, 0, 2);
        cvPutText(image, "Hand Gesture.", cvPoint(100, 260), &font, cvScalar(0, 0, 300));
        
        cvShowImage( "CamShiftDemo", image );
        

/* Salvar imagenes a un archivo       */


int keyCode = cvWaitKey(10);
if ('k' == (char)keyCode)
{label = 1;}
if (label == 1)
		{	
			static int imageCount = 0;
			char filename1[32];
                        char filename2[32];
                        char filename3[32];
			sprintf(filename1, "image%d.jpg", imageCount);
                        sprintf(filename2, "back%d.jpg", imageCount);
                        sprintf(filename3, "mask%d.jpg", imageCount);
			cvSaveImage(filename1, image);
                        cvSaveImage(filename2, maskara);
                        cvSaveImage(filename3, resize);
			++imageCount;}

/*      */

        cvReleaseImage( &tmp);
        cvReleaseImage( &temp);
        cvReleaseImage( &temporal);
        cvReleaseImage( &maskara);
        

        c = cvWaitKey(10);
        if( c == 27 )
            break;
        switch( c )
        {
        case 'b':
            backproject_mode ^= 1;
            break;
        case 'c':
            track_object = 0;
            break;
        
        default:
            ;
        }
    }

    cvReleaseCapture( &capture );
    cvDestroyWindow("CamShiftDemo");
    cvDestroyWindow( "Backprojection" );
    cvDestroyWindow( "mask" );
    cvDestroyWindow( "hue" );
    cvDestroyWindow( "sat" );
    cvDestroyWindow( "Floodfill" );

    return 0;
}

#ifdef _EiC
main(1,"Camshift2Dfinal.c");
#endif
