// g++ -I/usr/local/include/opencv -o objectmarker objectmarker.cpp -lcv -lhighgui

#include "cv.h"
#include "cvaux.h"
#include "highgui.h"

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>

using namespace std;

IplImage* image=0;
IplImage* image2=0;
int start_roi=0;
int roi_x0=0;
int roi_y0=0;
int roi_x1=0;
int roi_y1=0;
int numOfRec=0;
int mouseParam = 5;
char* window_name="OpenCV ObjectMarker: <SPACE> - add, <ENTER> - save and load next image, <ESC> - exit";
bool mousepressed = false;

void on_mouse(int event, int x, int y, int flag, void* param)
{
   	// printf("- %d %d", event, flag);
   	fflush(stdout);

	switch (event)
	{
      		case CV_EVENT_LBUTTONDOWN:
			start_roi=1;
	 		roi_x0=x;
	 		roi_y0=y;			
        	break;
      		case CV_EVENT_LBUTTONUP:
        		roi_x1=x;
			roi_y1=y;

	 		//redraw ROI selection
	 		image2=cvCloneImage(image);
	 		cvRectangle(image2,
			   cvPoint(roi_x0,roi_y0),
			   cvPoint(roi_x1,roi_y1),
			   CV_RGB(255,0,255),
			   1);
			
			// display image and rectangle
	 		cvShowImage(window_name,image2);
	 		cvReleaseImage(&image2);
        	break;
	}
}

int main(int argc, char** argv)
{
	char filename[255];			// name of the input positive image file
	char imagename[] = "frame"; 		// part of the input positive image file
	char imagedir[] = "/home/satoyiro/opencv-0.9.7/samples/c/Camshift";// directory where positive images are stored
	char output_file[] = "train.txt"; 	// name of the output file where image name and boundaries of the object will be stored
	int iKey=0;
	int start_index = 0;

	// parameter indicates at which file number to start
	// without parameter - starts at 0.
	if (argc == 2)
		start_index = atoi(argv[1]);
	
  	// init highgui
  	cvAddSearchPath("");
  	cvNamedWindow(window_name,1);
  	cvSetMouseCallback(window_name,on_mouse,&mouseParam);

  	// init output of rectangles to the info file
  	ofstream output(output_file, ios_base::out | ios_base::app);

  	char strPrefix[8000];
  	char strPostfix[8000];

  	for (int i = start_index; image = 0, sprintf(filename, "%s/%s%d.jpg", imagedir, imagename, i), 
		image = cvLoadImage(filename,1), image != 0; 
		i++) 
	{
    		printf("Now processing picture: %s \n", filename);

    		strcpy(strPrefix, filename);			
    		numOfRec=0;
			
	    	// work on current image
	   	do
		{
			cvShowImage(window_name,image);
			
	 	  	// used cvWaitKey returns:
	 	  	//	<Enter>=1048586, 1113997 save added rectangles and show next image
	 	  	//	<ESC>=1048603		 exit program
	 	  	//	<Space>=1048608		 add rectangle to current image

		   	iKey=cvWaitKey(0);

		   	switch(iKey)
			{
				case 27: // <ESC> key
					cvReleaseImage(&image);
					cvDestroyWindow(window_name);
			  		output.close();
					printf("File saved as %s\n", output_file);
			  		return(0);

				case 32: // <SPACE> key
			  		// currently two draw directions possible:
			  		// from top left to bottom right or vice versa
		  			if(roi_x0<roi_x1 && roi_y0<roi_y1)
		    			{
						numOfRec++;
				 		printf("   %d. rect x=%d y=%d width=%d height=%d\n",
						   numOfRec, roi_x0, roi_y0, roi_x1-roi_x0, roi_y1-roi_y0);

				 		// append rectangle coord to previous line content
			 			char app[100];
			 			sprintf(app,"%d %d %d %d ", roi_x0, roi_y0, roi_x1-roi_x0, roi_y1-roi_y0);
				 		strcat(strPostfix, app);
			    		}
			  		if(roi_x0>roi_x1 && roi_y0>roi_y1)
				    	{
						numOfRec++;
					 	printf("   %d. rect x=%d y=%d width=%d height=%d\n",
						   numOfRec, roi_x1, roi_y1, roi_x0-roi_x1, roi_y0-roi_y1);
	
					 	// append rectangle coord to previous line content
					 	char app[100];
					 	sprintf(app,"%d %d %d %d ", roi_x1, roi_y1, roi_x0-roi_x1, roi_y0-roi_y1);
					 	strcat(strPostfix, app);
				    	}
				break;
			}
		}
	    	while(iKey!=10 && iKey!=65421); // any <ENTER> key
		
		// if a rectange was selected then
    		// save to a file in a format: <path_to_image_file> <num_of_rects> <x0> <y0> <width> <height>
    		if(numOfRec>0)
		{
			//append line
		   	output << strPrefix << " " << numOfRec << " " << strPostfix << endl;
		   	printf("line appended: \n   %s %d %s\n", strPrefix, numOfRec, strPostfix);
		   	memset(strPostfix,0, 8000);
		}
		cvReleaseImage(&image);
	} // end for
	printf("Processed all images!\n");
	// close output file
  	output.close();
	printf("File saved as %s\n", output_file);
  	cvDestroyWindow(window_name);
  	return 0;
}
