#pragma once

#include <iostream>
#include <opencv2/core/mat.hpp>

using namespace cv;

namespace me {
double calculatSqrt(double number);

void img_processing(InputArray image, OutputArray output);
}
