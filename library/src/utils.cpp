// Some stuff
#include "utils.h"
#include <awesome.h>

#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

// #include <cstdlib>
#include <iostream>
#include <cmath>

namespace me {

double calculatSqrt(double number) {
  std::cout << "My Custom implementation of square root\n";
  ::awesome::mySqrt(number);
  return std::sqrt(number);
}

// cv::Ptr<cv::SIFT> siftPtr = cv::SIFT::create();
cv::Ptr<cv::xfeatures2d::SURF> _surfPtr = cv::xfeatures2d::SURF::create();


void img_processing(InputArray image, OutputArray output) {
  std::vector<cv::KeyPoint> keypoints;
  _surfPtr->detect(image, keypoints);
  // Add results to image and save.
  cv::Mat outputInt;
  cv::drawKeypoints(image, keypoints, outputInt);
  outputInt.copyTo(output);
}
}
